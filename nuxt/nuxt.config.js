module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  modules: [
    ['@nuxtjs/proxy', { pathRewrite: { '^/api/': '' } }],
    // ['@nuxtjs/proxy'],
  ],

  proxy: {
    // Simple proxy
    // '/api/users': 'http://localhost:3030/users',
    // '/api/auth': 'http://localhost:3030/authentication',
    '/api': 'http://localhost:3030',

    // '/api/users/': { target: 'http://localhost:3030/users/', ws: false }


  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        // config.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules)/
        // })
      }
    }
  }
}
